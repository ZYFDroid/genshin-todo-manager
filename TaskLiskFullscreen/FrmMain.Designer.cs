﻿namespace TaskLiskFullscreen
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.webViewMain = new Microsoft.Web.WebView2.WinForms.WebView2();
            this.animTimer = new System.Windows.Forms.Timer(this.components);
            this.hotkeyListener1 = new TaskLiskFullscreen.HotkeyListener();
            this.hotkeyTimer = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.显示界面ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.设置取消开机自启ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.webViewMain)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // webViewMain
            // 
            this.webViewMain.AllowExternalDrop = true;
            this.webViewMain.BackColor = System.Drawing.Color.White;
            this.webViewMain.CreationProperties = null;
            this.webViewMain.DefaultBackgroundColor = System.Drawing.Color.Transparent;
            this.webViewMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webViewMain.Location = new System.Drawing.Point(0, 0);
            this.webViewMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.webViewMain.Name = "webViewMain";
            this.webViewMain.Size = new System.Drawing.Size(682, 384);
            this.webViewMain.TabIndex = 0;
            this.webViewMain.ZoomFactor = 1D;
            this.webViewMain.CoreWebView2InitializationCompleted += new System.EventHandler<Microsoft.Web.WebView2.Core.CoreWebView2InitializationCompletedEventArgs>(this.webViewMain_CoreWebView2InitializationCompleted);
            // 
            // animTimer
            // 
            this.animTimer.Interval = 1;
            this.animTimer.Tick += new System.EventHandler(this.animTimer_Tick);
            // 
            // hotkeyListener1
            // 
            this.hotkeyListener1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hotkeyListener1.Location = new System.Drawing.Point(70, 40);
            this.hotkeyListener1.Name = "hotkeyListener1";
            this.hotkeyListener1.Size = new System.Drawing.Size(32, 32);
            this.hotkeyListener1.TabIndex = 1;
            // 
            // hotkeyTimer
            // 
            this.hotkeyTimer.Tick += new System.EventHandler(this.hotkeyTimer_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示界面ToolStripMenuItem,
            this.toolStripSeparator2,
            this.设置取消开机自启ToolStripMenuItem,
            this.toolStripSeparator1,
            this.退出ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(262, 118);
            // 
            // 显示界面ToolStripMenuItem
            // 
            this.显示界面ToolStripMenuItem.Name = "显示界面ToolStripMenuItem";
            this.显示界面ToolStripMenuItem.Size = new System.Drawing.Size(261, 34);
            this.显示界面ToolStripMenuItem.Text = "显示界面";
            this.显示界面ToolStripMenuItem.Click += new System.EventHandler(this.显示界面ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(258, 6);
            // 
            // 设置取消开机自启ToolStripMenuItem
            // 
            this.设置取消开机自启ToolStripMenuItem.Name = "设置取消开机自启ToolStripMenuItem";
            this.设置取消开机自启ToolStripMenuItem.Size = new System.Drawing.Size(261, 34);
            this.设置取消开机自启ToolStripMenuItem.Text = "设置/取消开机自启";
            this.设置取消开机自启ToolStripMenuItem.Click += new System.EventHandler(this.设置取消开机自启ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(258, 6);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(261, 34);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Aqua;
            this.ClientSize = new System.Drawing.Size(682, 384);
            this.Controls.Add(this.hotkeyListener1);
            this.Controls.Add(this.webViewMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmMain";
            this.Opacity = 0.01D;
            this.ShowInTaskbar = false;
            this.Text = "任务列表";
            this.TransparencyKey = System.Drawing.Color.Aqua;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMain_FormClosed);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.webViewMain)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Web.WebView2.WinForms.WebView2 webViewMain;
        private System.Windows.Forms.Timer animTimer;
        private HotkeyListener hotkeyListener1;
        private System.Windows.Forms.Timer hotkeyTimer;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 显示界面ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 设置取消开机自启ToolStripMenuItem;
    }
}

