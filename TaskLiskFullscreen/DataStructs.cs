﻿using LiteDB;
using Microsoft.Web.WebView2.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TaskLiskFullscreen
{
    public class TodoTask
    {
        public string _id { get; set; } = ObjectId.NewObjectId().ToString();
        public string Title { get; set; }
        public string Content { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public DateTime? EndDate { get; set;}
        public DateTime? CompleteTime { get; set; }

        public List<RelatedFile> RelatedFiles { get; set; }  = new List<RelatedFile>();

        public string Location { get; set; }

        public int TypeLevel { get; set;}

        public bool IsCompleted { get; set; }

    }

    public class RelatedFile
    {
        public string DisplayName { get; set; }
        public string Path { get; set; }
        public bool IsDir { get; set; }
    }



    public class TodoTaskManager
    {
        private LiteDatabase db;
        private ILiteCollection<TodoTask> col;
        public TodoTaskManager() {
            db = new LiteDatabase("app/app.ltdb");
            col = db.GetCollection<TodoTask>("task");
            if (Program.IsFirstLaunch)
            {
                TodoTask newtask = new TodoTask();
                newtask.Location = "ZYFDroid";
                newtask.Title = "欢迎使用 Genshin Todo Manager";
                newtask.ModifiedDate = DateTime.Now;
                newtask.Content = @"
## 欢迎使用 Genshin Todo Manager
<br>
<br>
一个原神任务界面样式的待办管理工具。

<br>

- 支持将任务分成四个类型。
- 按下 `Win`+`J` 可以呼出界面，按下 `Esc` 键快速关闭。
- 任务描述支持Markdown。
- 支持设置任务截止时间。
- 支持添加快捷打开文件的方式。

---

### 常见问题

- 使用部分应用程序或游戏时无法用快捷键呼出界面。请使用管理员权限运行。
- 如果界面显示错误或者跳转到了不存在的地方，可以按`F5`刷新或者右键后退

---

### 更多信息

> 源代码：[https://gitee.com/ZYFDroid/genshin-todo-manager](https://gitee.com/ZYFDroid/genshin-todo-manager)

";
                newtask.Content = newtask.Content.Trim();
                newtask.TypeLevel = 1;
                newtask.RelatedFiles = new List<RelatedFile>();

                AddTask(newtask);

                newtask = new TodoTask();
                newtask.TypeLevel = 4;
                newtask.Title = "设置开机自启动"; 
                newtask.Location = "ZYFDroid";
                newtask.ModifiedDate = DateTime.Now;
                newtask.EndDate = DateTime.Now.AddDays(10);
                newtask.RelatedFiles = new List<RelatedFile>();
                string taskschd = Environment.ExpandEnvironmentVariables("%windir%\\System32\\taskschd.msc");

                newtask.RelatedFiles.Add(new RelatedFile() {
                    Path=taskschd,
                    DisplayName="任务计划程序 (taskschd.msc)",
                    IsDir = false,
                });

                newtask.Content = @"

> 将软件设置开机自启，或者，直接完成本条待办 (如果不需要自启)

按 Esc 关闭界面，在右下角打开托盘，找到软件的图标，右键选择`设置/取消开机自启`。

如果弹出了是否以管理员权限运行的对话框，请点击允许。

---

如果不再需要开机自启，请重复上面的操作关闭开机自启。如果移动了软件的位置，请取消开机自启后重新添加。

你可以在[任务计划程序](/api/todo/v2/openfile?{TASKSCHD})中找到本软件的开机自启项。


".Replace("{TASKSCHD}", HttpUtility.UrlEncode(taskschd)).Trim(); ;
                AddTask(newtask);
            }
        }

        public void CloseDB()
        {
            db.Dispose();
        }

        public List<TodoTask> GetAllTask() {
            var taskA = col.Query().Where(t => t.IsCompleted == false).OrderByDescending(t => t.ModifiedDate).ToList();
            taskA.AddRange(GetCompletedTask());
            return taskA;
        }

        public List<TodoTask> GetTaskByType(int type) {
            return col.Query().Where(q => q.TypeLevel == type && q.IsCompleted == false).ToList();
        }

        public List<TodoTask> GetCompletedTask()
        {
            return col.Query().Where(q => q.IsCompleted).OrderByDescending(t => t.CompleteTime).Limit(64).ToList();
        }

        public void AddTask(TodoTask task) { 
            task.ModifiedDate = DateTime.Now;
            task._id = ObjectId.NewObjectId().ToString();
            col.Insert(task);
        }

        public void EditTask(TodoTask task)
        {
            col.Update(task);
        }

        internal void DeleteTask(TodoTask task)
        {
            col.Delete(task._id);
        }

        internal void CompleteTask(TodoTask task)
        {
            task.IsCompleted = true;
            task.CompleteTime = DateTime.Now;
            col.Update(task);
        }
    }
}
