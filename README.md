# GenshinTodoManager

> 本项目仅供学习交流使用，可以用于学习CSS、Vue、C#图像处理、WebView2二次开发、PPT的使用等。禁止商用
> 素材均为ppt自行绘制，侵删

#### 介绍

仿原神任务页面的待办管理器

支持分不同等级的任务

支持任务到期时间

支持查看已完成的任务

支持在任务详情中使用markdown

![preview](preview.png)